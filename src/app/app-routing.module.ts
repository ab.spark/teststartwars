import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WipComponent} from "./shared/component/wip/wip.component";

const routes: Routes = [
  {
    path: ':topic',
    loadChildren: () => import('./main-features/topic/topic.module').then((m => m.TopicModule))
  },
  {
    path: '**',
    component: WipComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
