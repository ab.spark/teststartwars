import {ComponentFixture, TestBed} from '@angular/core/testing';
import {HeaderComponent} from './header.component';
import {Observable, of} from "rxjs";
import {HeaderService} from "../services/header.service";
import {SharedService} from "../../services/shared.service";
import {ActivatedRoute} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {searchResult} from "../../../core/config/mocks/searchResult";
import {BrowserDynamicTestingModule} from "@angular/platform-browser-dynamic/testing";

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let headerMockService: Partial<HeaderService> = {
    search(topic: string, keyword: string): Observable<Object> {
      return of(searchResult)
    }
  };
  let sharedMockService: SharedService = new SharedService();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule, BrowserDynamicTestingModule],
      declarations: [HeaderComponent],
      providers: [
        {provide: SharedService, useValue: sharedMockService},
        {provide: HeaderService, useValue: headerMockService},
        {provide: 'searchHistoryItems', useValue: 3},
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              topic: 'people',
            }),
          }
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    const searchButton = fixture.debugElement.nativeElement.querySelector('button');
    searchButton.click();
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialise search form', () => {
    expect(component.searchForm.get('search')).toBeTruthy();
  });

  it('should get active topic', () => {
    expect(component.activeTopic).toBeTruthy();
    expect(component.activeTopic).toEqual('people');
  });

  it('should be valid search form', () => {
    expect(component.searchForm.valid).toEqual(true);
  })

  it('should trigger search and update shared service', () => {
    sharedMockService.getSearchResult().subscribe((searchResult) => {
      expect(searchResult.results.length).toEqual(1);
      expect(searchResult.results[0].name).toEqual('Luke Skywalker');
    })
  });


});
