import {ChangeDetectionStrategy, Component, Injector, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {HeaderService} from "../services/header.service";
import {ActivatedRoute} from "@angular/router";
import {SharedService} from "../../services/shared.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit {

  storedSearchHistory = [];
  searchForm: FormGroup = new FormGroup({});
  activeTopic = '';

  constructor(private injector: Injector,
              private fb: FormBuilder,
              private headerService: HeaderService,
              private sharedService: SharedService,
              private router: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.storedSearchHistory = JSON.parse(localStorage.getItem('searchHistory') || "[]");
    this.initSearchForm();
    this.getActiveTopic();
  }

  initSearchForm() {
    this.searchForm = this.fb.group({
      search: new FormControl('', [])
    })
  }

  getActiveTopic() {
    this.router.params.subscribe((params: any) => {
      this.activeTopic = params['topic'];
    })
  }

  triggerSearch() {
    if (this.searchForm.valid) {
      this.handleSearchHistory(this.searchForm.get('search')?.value, this.injector.get('searchHistoryItems'));
      this.headerService.search(this.activeTopic, this.searchForm.get('search')?.value).subscribe((result) => {
        this.sharedService.setSearchResult(result);
      })
    }
  }

  handleSearchHistory(keyword: string, historySize: number) {
    if (keyword) {
      let activeHistory = localStorage.getItem('searchHistory') as string;
      const historyArray = JSON.parse(activeHistory) || [];
      if (historyArray.length == historySize) {
        historyArray.shift();
      }
      historyArray.push(keyword);
      this.storedSearchHistory = historyArray;
      localStorage.setItem('searchHistory', JSON.stringify(historyArray));
    }
  }

}
