import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  constructor(private http: HttpClient) {
  }

  search(topic: string, keyword: string): Observable<any> {
    return this.http.get(`${environment.baseURL}/api/${topic}/?search=${keyword}`)
  }

}
