import { TestBed } from '@angular/core/testing';

import { HeaderService } from './header.service';
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe('HeaderService', () => {
  let service: HeaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(HeaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
