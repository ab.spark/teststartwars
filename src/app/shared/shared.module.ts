import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WipComponent} from "./component/wip/wip.component";
import {NavigationMenuComponent} from './component/navigation-menu/navigation-menu.component';
import {RouterModule} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    WipComponent,
    NavigationMenuComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule
  ],
  exports: [
    WipComponent,
    NavigationMenuComponent
  ]
})
export class SharedModule {
}
