import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  searchResult : BehaviorSubject<any> = new BehaviorSubject<any>({})

  constructor() { }

  setSearchResult(result: any){
    this.searchResult.next(result);
  }

  getSearchResult(){
    return this.searchResult.asObservable()
  }


}
