import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {TopicsService} from "../services/topics.service";
import {SharedService} from "../../../shared/services/shared.service";
import {Location} from "@angular/common";
import {map} from "rxjs";

@Component({
  selector: 'app-topic-details',
  templateUrl: './topic-details.component.html',
  styleUrls: ['./topic-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopicDetailsComponent implements OnInit {

  topicItem:any;
  topicDetails: any[] = [];
  topicName: any;

  constructor(private route: ActivatedRoute,
              private topicsService: TopicsService,
              private cdr: ChangeDetectorRef,
              private sharedService: SharedService,
              private location: Location) {
  }

  ngOnInit(): void {
    this.getActiveTopicItem();
  }

  getActiveTopicItem() {
    this.route.params.subscribe((params) => {
      this.topicName = params['topic']
      this.fetchTopicItem(params['topic'],params['id']);
    })
  }

  fetchTopicItem(topicItem: string, topicId : number) {
    this.topicsService.getTopicItem(topicItem, topicId).pipe(map((element:any)=>{
      if (this.topicName == 'people') {
        element.height > 200 ? element.height = 'hight' : element.height < 100 ? element.height = 'low' : element.height = 'normal';
      }
      return element;
    })).subscribe((result:any) => {
      this.topicItem = result;
      this.topicDetails = [];
      for(let key in result){
        this.topicDetails.push(key);
      }
      this.cdr.detectChanges();
    })
  }

  goBack(){
    this.location.back()
  }

}
