import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TopicDetailsComponent} from './topic-details.component';
import {RouterTestingModule} from "@angular/router/testing";
import {TopicsService} from "../services/topics.service";
import {SharedService} from "../../../shared/services/shared.service";
import {ActivatedRoute} from "@angular/router";
import {Observable, of} from "rxjs";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {peapleItem} from "../../../core/config/mocks/peapleItem";

describe('TopicDetailsComponent', () => {
  let component: TopicDetailsComponent;
  let fixture: ComponentFixture<TopicDetailsComponent>;
  let topicsMockService: Partial<TopicsService> = {
    getTopicItem(topic: string, id: number): Observable<Object> {
      return of(peapleItem)
    }
  };
  let sharedMockService: SharedService = new SharedService();
  let loc: Location;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TopicDetailsComponent],
      imports: [RouterTestingModule],
      providers: [
        {provide: TopicsService, useValue: topicsMockService},
        {provide: SharedService, useValue: sharedMockService},
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              topic: 'people',
              id: 1
            }),
          },
        }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get active topic', () => {
    expect(component.topicName).toBeTruthy();
    expect(component.topicName).toEqual('people');
  });

  it('should get topic specific item and handle mapping', () => {
    component.fetchTopicItem(component.topicName, 1);
    expect(component.topicItem).toBeTruthy();
    expect(component.topicItem.url).toContain('/1/')
  })
});
