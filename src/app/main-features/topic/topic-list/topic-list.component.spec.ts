import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TopicListComponent} from './topic-list.component';
import {TopicsService} from "../services/topics.service";
import {Observable, of} from "rxjs";
import {peapleList} from "../../../core/config/mocks/peapleList";
import {ActivatedRoute} from "@angular/router";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {SharedService} from "../../../shared/services/shared.service";
import {searchResult} from "../../../core/config/mocks/searchResult";
import {RouterTestingModule} from "@angular/router/testing";

describe('FilmsListComponent', () => {
  let component: TopicListComponent;
  let fixture: ComponentFixture<TopicListComponent>;
  let topicsMockService: Partial<TopicsService> = {
    getTopicItems(topic: string, page: number): Observable<Object> {
      return of(peapleList)
    }
  };
  let sharedMockService: SharedService = new SharedService();
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TopicListComponent],
      imports : [RouterTestingModule],
      providers: [
        {provide: TopicsService, useValue: topicsMockService},
        {provide: SharedService, useValue: sharedMockService},
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              topic: 'people',
            }),
          },
        }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should get active topic', () => {
    expect(component.topicName).toBeTruthy();
    expect(component.topicName).toEqual('people');
  });

  it('should get topic items and handle mapping', () => {
    component.fetchTopicItems();
    expect(component.topics).toBeTruthy();
    expect(component.topics.results.length).toEqual(10)
    expect(component.topics.results[0].height).toEqual('normal')
  })

  it('should handle search filter', () => {
    sharedMockService.setSearchResult(searchResult);
    component.handleSearchFilter();
    expect(component.topics).toBeTruthy();
    expect(component.topics.results.length).toEqual(1)
  })

});
