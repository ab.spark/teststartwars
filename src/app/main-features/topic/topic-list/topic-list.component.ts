import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {TopicsService} from "../services/topics.service";
import {map, Subject, takeUntil} from "rxjs";
import {SharedService} from "../../../shared/services/shared.service";

@Component({
  selector: 'app-topic-list',
  templateUrl: './topic-list.component.html',
  styleUrls: ['./topic-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopicListComponent implements OnInit, OnDestroy {

  $destroyed = new Subject();
  topicName = '';
  topics: any = {}
  page = 1;

  constructor(private route: ActivatedRoute, private topicsService: TopicsService, private cdr: ChangeDetectorRef, private sharedService: SharedService) {
  }

  ngOnDestroy(): void {
    this.$destroyed.next(true);
  }

  ngOnInit(): void {
    this.getActiveTopic();
    this.handleSearchFilter();
  }

  getActiveTopic() {
    this.route.params.pipe(takeUntil(this.$destroyed)).subscribe((params) => {
      this.topicName = params['topic'];
      this.fetchTopicItems();
    })
  }

  handleSearchFilter() {
    this.sharedService.getSearchResult().pipe(takeUntil(this.$destroyed)).subscribe((result) => {
      this.topics = result;
      this.cdr.detectChanges();
    })
  }

  nextPage() {
    if (this.topics.next) {
      this.page++;
      this.fetchTopicItems();
    }
  }

  previousPage() {
    if (this.topics.previous) {
      this.page--;
      this.fetchTopicItems();
    }
  }

  fetchTopicItems() {
    this.topicsService.getTopicItems(this.topicName, this.page).pipe(map((result: any) => {
      result.results.map((element: any) => {
        if (this.topicName == 'people') {
          element.height > 200 ? element.height = 'hight' : element.height < 100 ? element.height = 'low' : element.height = 'normal';
        }
        const path = new URL(element.url).pathname.split('/');
        element.id = path[path.length - 2]
        return element;
      })
      return result;
    }, takeUntil(this.$destroyed))).subscribe((result) => {
      this.topics = result;
      this.cdr.detectChanges();
    })
  }

}
