import { TestBed } from '@angular/core/testing';

import { TopicsService } from './topics.service';
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe('TopicsService', () => {
  let service: TopicsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(TopicsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
