import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class TopicsService {

  constructor(private http: HttpClient) {
  }

  getTopicItem(topic: string, id: number) {
    return this.http.get(`${environment.baseURL}/api/${topic}/${id}`)
  }

  getTopicItems(topic: string, page: number) {
      return this.http.get(`${environment.baseURL}/api/${topic}/?page=${page}`)
  }
}
