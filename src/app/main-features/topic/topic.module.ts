import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TopicListComponent} from './topic-list/topic-list.component';
import {LayoutModule} from "../../shared/layout/layout.module";
import {SharedModule} from "../../shared/shared.module";
import {TopicRoutingModule} from "./topic-routing.module";
import { TopicDetailsComponent } from './topic-details/topic-details.component';

@NgModule({
  declarations: [
    TopicListComponent,
    TopicDetailsComponent
  ],
  imports: [
    CommonModule,
    TopicRoutingModule,
    LayoutModule,
    SharedModule
  ]
})
export class TopicModule {
}
