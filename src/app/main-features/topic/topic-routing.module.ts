import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TopicListComponent} from "./topic-list/topic-list.component";
import {TopicDetailsComponent} from "./topic-details/topic-details.component";

const routes: Routes = [
  {
    path: '',
    component : TopicListComponent
  },
  {
    path: ':id',
    component : TopicDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TopicRoutingModule { }
